package fieldutils

import (
	"fmt"
	"reflect"
	"strconv"
	"strings"
)

func DefaultBlank(value interface{}) string {
	if value == nil {
		return ""
	}

	return interfaceToString(value)
}

// Deprecated
func DefaultArray(val interface{}) []string {
	return DefaultStringArray(val)
}

func DefaultStringArray(val interface{}) []string {
	if val == nil {
		return nil
	}

	return val.([]string)
}

func DefaultMap(val interface{}) map[string]interface{} {
	if val == nil {
		return nil
	}
	return val.(map[string]interface{})
}

func DefaultZeroInt(val interface{}) int {

	if val == nil {
		return 0
	}
	switch v := val.(type) {
	case float64:
		return int(v)
	case string:
		result, err := strconv.Atoi(v)
		if err != nil {
			return 0
		}
		return result
	case []byte:
		result, err := strconv.ParseFloat(string(v), 64)
		if err != nil {
			return 0
		}
		return int(result)
	case int64:
		result := val.(int64)
		return int(result)
	default:
		panic(fmt.Sprintf("unexpected type %T", v))
	}

}

// DefaultZeroFloat returns 0 if the value is nil or an empty string
// otherwise it returns the value as a float64
func DefaultZeroFloat(value interface{}) float64 {
	if value == nil {
		return 0
	}
	switch v := value.(type) {
	case float64:
		return v
	case float32:
		return float64(v)
	case string:
		result, err := strconv.ParseFloat(v, 64)
		if err != nil {
			return 0
		}
		return result
	case []byte:
		result, err := strconv.ParseFloat(string(v), 64)
		if err != nil {
			return 0
		}
		return result
	default:
		panic(fmt.Sprintf("unexpected type %T", v))
	}
}

func DefaultTrue(val interface{}) bool {
	if val == nil {
		return true
	}

	if reflect.TypeOf(val).String() == "string" {
		ans, err := strconv.ParseBool(val.(string))
		if err != nil {
			return true
		}
		return ans
	}
	return val.(bool)
}

func DefaultFalse(val interface{}) bool {
	if val == nil {
		return false
	}

	if reflect.TypeOf(val).String() == "string" {
		ans, err := strconv.ParseBool(val.(string))
		if err != nil {
			return false
		}
		return ans
	}
	return val.(bool)
}

func DefaultString(val interface{}, def string) string {
	if val == nil {
		return def
	}

	return interfaceToString(val)
}

func interfaceToString(value interface{}) string {
	switch v := value.(type) {
	case string:
		return strings.Trim(v, " ")

	case []uint8:
		return string(value.([]byte))

	default:
		panic(fmt.Sprintf("unexpected type %T", v))
	}
}
