package fieldutils

import (
	"errors"
	"strings"
)

func FieldValidation(res []byte, fieldList []string) error {
	str := string(res)
	for _, itm := range fieldList {
		if !strings.Contains(str, itm) {
			return errors.New("missing field:" + itm)
		}
	}
	return nil
}
