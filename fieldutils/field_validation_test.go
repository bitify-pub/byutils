package fieldutils

import (
	"testing"
)

func TestValidateField(t *testing.T) {
	tests := []struct {
		name      string
		res       []byte
		fieldList []string
		wantErr   bool
		wantMsg   string
	}{
		{
			name:      "All fields present",
			res:       []byte(`{"field1": "value1", "field2": "value2", "field3": "value3"}`),
			fieldList: []string{"field1", "field2", "field3"},
			wantErr:   false,
		},
		{
			name:      "Missing one field",
			res:       []byte(`{"field1": "value1", "field2": "value2"}`),
			fieldList: []string{"field1", "field2", "field3"},
			wantErr:   true,
			wantMsg:   "missing field:field3",
		},
		{
			name:      "Empty field list",
			res:       []byte(`{"field1": "value1"}`),
			fieldList: []string{},
			wantErr:   false,
		},
		{
			name:      "Empty response",
			res:       []byte(`{}`),
			fieldList: []string{"field1"},
			wantErr:   true,
			wantMsg:   "missing field:field1",
		},
		{
			name:      "Field list is empty and response is empty",
			res:       []byte(`{}`),
			fieldList: []string{},
			wantErr:   false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			err := FieldValidation(tt.res, tt.fieldList)
			if (err != nil) != tt.wantErr {
				t.Errorf("validateField() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if err != nil && err.Error() != tt.wantMsg {
				t.Errorf("validateField() error message = %v, wantMsg %v", err.Error(), tt.wantMsg)
			}
		})
	}

	t.Run("testFieldValidation", func(t *testing.T) {

		res := []byte(`{
			"db.host":"mail.com"
			"db.port":"123"
		}`)

		liststr := []string{"db.host", "db.port"}

		err := FieldValidation(res, liststr)
		if err != nil {
			t.Fail()
		}

	})

}
