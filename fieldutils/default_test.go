package fieldutils

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
)

func TestDefaultTrue(t *testing.T) {

	got := DefaultTrue("true")
	want := true

	if got != want {
		t.Errorf("got %t, wanted %t", got, want)
	}

	got2 := DefaultTrue("trues")
	want2 := true

	if got2 != want2 {
		t.Errorf("got %t, wanted %t", got2, want2)
	}

	got3 := DefaultTrue("spelling error")
	want3 := true

	if got3 != want3 {
		t.Errorf("got %t, wanted %t", got3, want3)
	}

	got4 := DefaultTrue(false)
	want4 := false

	if got4 != want4 {
		t.Errorf("got %t, wanted %t", got4, want4)
	}

	got5 := DefaultTrue("false")
	want5 := false

	if got5 != want5 {
		t.Errorf("got %t, wanted %t", got5, want5)
	}

}

func TestDefaultFloat(t *testing.T) {

	var val_1 interface{} = []byte("3.1415")
	var val_2 interface{} = &val_1

	xx := *val_2.(*interface{})

	fmt.Println(xx)

	got := DefaultZeroFloat(xx)

	fmt.Println("got", got)
}

func TestDefaultBlank(t *testing.T) {

	t.Run("defaultBlank", func(t *testing.T) {
		var val_1 interface{} = []uint8("3.1415")
		var val_2 interface{} = &val_1

		//source of testing
		// productinfo := (*vals[PRODNO2].(*interface{})).(string)

		// panic: interface conversion: interface {} is []uint8, not string [recovered]
		// panic: interface conversion: interface {} is []uint8, not string
		// xx := (*val_2.(*interface{})).(string)
		expect.Error(t, *val_2.(*interface{})).ToBe([]uint8("3.1415"))

		var val_3 interface{} = []byte("3.1415")
		var val_4 interface{} = &val_3

		expect.Value(t, "default blank should accept []uint8", DefaultBlank(*val_2.(*interface{}))).ToBe("3.1415")
		expect.Value(t, "default blank should accept []byte", DefaultBlank(*val_4.(*interface{}))).ToBe("3.1415")
	})

	t.Run("defaultZeroInt", func(t *testing.T) {
		var val_1 interface{} = []uint8("35.00")
		var val_2 interface{} = &val_1

		expect.Value(t, "default zero should accept []uint8", DefaultZeroInt(*val_2.(*interface{}))).ToBe(35)
		// expect.Value(t, "default blank should accept []byte", DefaultBlank(*val_4.(*interface{}))).ToBe("3.1415")
	})
}
