package fieldutils

// return float64 in 2 decimals
func Round2Decimal(val float64) float64 {

	d := (val * 1000) + 5
	var f int
	f = int(d / 10)
	return float64(f) / 100.0

}
