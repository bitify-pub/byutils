package httputils

import (
	"encoding/json"
	"net/http"

	"github.com/no-src/log"
)

// / ReportError writes an error message to the response writer
func ReportError(w http.ResponseWriter, err error, statusCode int) {
	msg := map[string]string{"error": err.Error()}
	log.Error(err, "")
	bmsg, _ := json.Marshal(msg)

	w.WriteHeader(statusCode)
	w.Header().Set("Content-Type", "application/json")
	w.Write(bmsg)
}

func AddSafeHeaders(w http.ResponseWriter) {
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("X-XSS-Protection", "1; mode=block")
	w.Header().Set("X-Frame-Options", "SAMEORIGIN")
	w.Header().Set("Strict-Transport-Security", "max-age=2592000; includeSubDomains")
}
