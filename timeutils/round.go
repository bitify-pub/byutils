package timeutils

import (
	"time"
)

/// RoundDownTo5Minutes rounds down a time to the previous 5 minutes.
/// Eg: 12:04:00 -> 12:00:00
func RoundDownTo5Minutes(t time.Time) time.Time {

	diff := t.Minute() % 5

	return t.Add(-time.Duration(diff) * time.Minute).Truncate(5 * time.Minute)
}