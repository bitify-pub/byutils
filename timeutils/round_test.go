package timeutils

import (
	"fmt"
	"testing"
	"time"
)

func TestRound(t *testing.T) {
	tm := time.Date(2020, 1, 1, 12, 4, 0, 0, time.UTC)

	rounded := RoundDownTo5Minutes(tm)

	if rounded.Minute() != 0 {
		t.Errorf("Expected 0, got %d", rounded.Minute())
	}

	// debugging code
	fmt.Println("Original time: ", tm)
	fmt.Println("Rounded time: ", rounded)
	t.Fail()
}
