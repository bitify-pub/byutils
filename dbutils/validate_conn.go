package dbutils

import (
	"errors"
	"fmt"
	"io"
	"net/http"
	"net/url"
)

// ValidateCouchdbUri checks uri structure and connectivity. It returns error if uri is invalid.
// uri format: http://username:password@host:port
func ValidateCouchdbUri(uri string) error {
	// check uri structure
	st, err := url.Parse(uri)
	if err != nil {
		return err
	}

	_, isset := st.User.Password()
	if !isset {
		return fmt.Errorf("password not set")
	}

	// check connectivity
	resp, err := http.Get(uri)
	if err != nil {
		return err
	}

	if resp.StatusCode != http.StatusOK {
		rb, err := io.ReadAll(resp.Body)
		if err != nil {
			return err
		}

		return errors.New(string(rb))
	}
	
	return nil
}
