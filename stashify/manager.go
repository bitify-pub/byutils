package stashify

import (
	"encoding/binary"
	"sync"
	"time"
)

/*Manage in-memory versus on disk caching*/

// CacheManager is the interface that wraps the basic cache operations.
// but must be hidden from the user

/* The cache manager maintains requests counts and promotes the most frequently requested items to memory.

   In memory cache must not exceed
*/

var instance *stashManager
var instMutext sync.Mutex
var maxMemSize int = 1_000_000 // 1MB

type stashManager struct {
	memstore  MemoryStore
	diskstore DiskStore
	stats     stashStats
	usedmem   int
}

func newStashManager() *stashManager {
	if instance == nil {
		instMutext.Lock()
		defer instMutext.Unlock()

		// check again after lock
		if instance != nil {
			return instance
		}

		instance = &stashManager{
			stats:     newStashStats(),
			usedmem:   0,
			memstore:  NewMemoryStore(),
			diskstore: NewDiskStore(),
		}
	}
	return instance
}

func (sm *stashManager) put(cacheKey string, data []byte, expire time.Duration) {
	sm.diskstore.Put(cacheKey, data, expire)

	sm.putmem(cacheKey, data, expire)
}

func (sm *stashManager) putmem(cacheKey string, data []byte, expire time.Duration) {
	if found := sm.stats.GetTopN(cacheKey); found {
		// put in memory if it is in top3
		sm.memstore.Put(cacheKey, data, expire)
		sm.usedmem += binary.Size(data)
	} else if sm.usedmem+binary.Size(data) < maxMemSize {
		// put in memory if we still have space
		sm.memstore.Put(cacheKey, data, expire)
		sm.usedmem += binary.Size(data)
	}
}

func (sm *stashManager) get(cacheKey string) ([]byte, bool) {
	sm.stats.inc(cacheKey)

	// get from memory
	if cacheddata, found := sm.memstore.Get(cacheKey); found {
		return cacheddata, true
	}

	if cacheddata, found := sm.diskstore.Get(cacheKey); found {
		go func() {
			data := sm.diskstore.GetData(cacheKey)
			if data == nil {
				return
			}
			sm.putmem(cacheKey, cacheddata, data.ExpireTime.Sub(time.Now()))
		}()
		return cacheddata, true
	}
	return nil, false
}

func (sm *stashManager) delete(cacheKey string) {
	sm.memstore.delete(cacheKey)
	sm.diskstore.Delete(cacheKey)
}

func (sm *stashManager) deleteWithPrefix(cachePrefix string) {
	sm.memstore.deleteWithPrefix(cachePrefix)
	sm.diskstore.DeleteWithPrefix(cachePrefix)
}
