package boltdb

import (
	"fmt"
	"testing"

	"github.com/akabio/expect"
	"github.com/no-src/nscache/encoding"
	"go.etcd.io/bbolt"
)

func TestRemoveWithPrefix(t *testing.T) {

	t.Run("testRemoveWithPrefix", func(t *testing.T) {

		db, err := bbolt.Open("boltdb.db", 0600, nil)
		if err != nil {
			fmt.Println(err)
		}

		err = db.Update(func(tx *bbolt.Tx) error {
			root, err := tx.CreateBucket([]byte("DB"))
			if err != nil {
				fmt.Println(err)
			}
			for i := 0; i < 10; i++ {
				key := fmt.Sprintf("key %d", i)
				val := fmt.Sprintf("something %d", i)
				root.Put([]byte(key), []byte(val))
			}

			for i := 0; i < 10; i++ {
				key := fmt.Sprintf("todelkey %d", i)
				val := fmt.Sprintf("todelsomething %d", i)
				root.Put([]byte(key), []byte(val))
			}

			return nil
		})
		if err != nil {
			fmt.Println(err)
		}
		err = db.View(func(tx *bbolt.Tx) error {
			b := tx.Bucket([]byte("DB"))
			count := 0
			b.ForEach(func(k, v []byte) error {
				count++
				return nil
			})
			fmt.Println(count)
			expect.Value(t, "inserted data should have 20", count).ToBe(20)
			return nil
		})

		dbstore := newStore(db, []byte("DB"), encoding.DefaultSerializer)

		dbstore.RemoveWithPrefix("todelkey")

		err = db.View(func(tx *bbolt.Tx) error {
			b := tx.Bucket([]byte("DB"))
			count := 0
			b.ForEach(func(k, v []byte) error {
				count++
				return nil
			})
			fmt.Println(count)
			expect.Value(t, "balance data should have 10", count).ToBe(10)
			return nil
		})

	})
	t.Fail()
}
