package boltdb

import (
	"time"

	"github.com/no-src/nscache"
	"github.com/no-src/nscache/encoding"
	"gitlab.com/bitify-pub/byutils/stashify/store"
)

// boltCache is the boltdb cache implementation
// inspired by nscache.cache
type boltCache struct {
	serializer encoding.Serializer
	store      store.Store
}

// NewCache create an instance of NSCache with the Store instance
func NewCache(store store.Store) (*boltCache, error) {
	c := &boltCache{
		serializer: encoding.DefaultSerializer,
		store:      store,
	}
	return c, nil
}

func (c *boltCache) Get(k string, v any) error {
	md := c.store.Get(k)
	if md == nil {
		return nscache.ErrNil
	}
	if md.IsExpired() {
		go c.Remove(k)
		return nscache.ErrNil
	}
	return c.serializer.Deserialize(md.Data, &v)
}

func (c *boltCache) Set(k string, v any, expiration time.Duration) error {
	data, err := c.serializer.Serialize(v)
	if err != nil {
		return err
	}
	return c.store.Set(k, data, expiration)
}

func (c *boltCache) Remove(k string) error {
	return c.store.Remove(k)
}

func (c *boltCache) RemoveWithPrefix(k string) error {
	return c.store.RemoveWithPrefix(k)
}

func (c *boltCache) Close() error {
	return c.store.Close()
}

func (c *boltCache) GetStore() store.Store {
	return c.store
}
