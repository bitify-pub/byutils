package boltdb

import (
	"net/url"

	"github.com/no-src/nscache/encoding"
	"go.etcd.io/bbolt"
)

const (
	// DriverName the unique name of the boltdb driver for register
	DriverName = "boltdb"
	// defaultBucket the default bucket name for boltdb
	defaultBucket = "nscache-default"
)

func NewBoltDBStore(conn string) (*boltCache, error) {
	path, bucket, err := parseConnection(conn)
	if err != nil {
		return nil, err
	}
	db, err := bbolt.Open(path, 0600, nil)
	if err != nil {
		return nil, err
	}
	return NewCache(newStore(db, []byte(bucket), encoding.DefaultSerializer))
}

// parseConnection parse the boltdb connection string
func parseConnection(conn string) (path string, bucket string, err error) {
	u, err := url.Parse(conn)
	if err != nil {
		return "", "", err
	}

	path = u.Host
	bucket = u.Query().Get("bucket")
	if len(bucket) == 0 {
		bucket = defaultBucket
	}
	return path, bucket, nil
}
