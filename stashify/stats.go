package stashify

import (
	"sync"
)

// Statistics for promoting or demoting cached data between
// in-memory and on-disk storage.
type stashStats struct {
	topNget  map[string]int
	countget map[string]int
}

func newStashStats() stashStats {
	return stashStats{
		topNget:  make(map[string]int),
		countget: make(map[string]int),
	}
}

var topN = 10
var incMut sync.Mutex = sync.Mutex{}

// inc increments the count of get requests for a cache key.
func (ss *stashStats) inc(key string) {

	incMut.Lock()
	defer incMut.Unlock()

	ss.countget[key]++

	// update topN
	if len(ss.topNget) < topN {
		ss.topNget[key] = ss.countget[key]
	} else {
		// find the smallest value in topN and replace it with the new key

		// find the key of smallest value in topN
		smallestkey := ""
		min := int(^uint(0) >> 1) // max int
		for sk, v := range ss.topNget {
			if v < min {
				smallestkey = sk
				min = v
			}
		}

		if ss.countget[key] > min {
			delete(ss.topNget, smallestkey)
			ss.topNget[key] = ss.countget[key]
		}
	}
}

func (ss *stashStats) GetTopN(cacheKey string) bool {
	incMut.Lock()
	defer incMut.Unlock()

	if _, found := ss.topNget[cacheKey]; found {
		return true
	}
	return false
}

func (sm *stashManager) info() {
	println("cache statistics: ")
	for k, v := range sm.stats.countget {
		println("\t", k, v, "size: ", sm.memstore.ByteSize(), "bytes")
	}

	print("top: %d\n", topN)
	for k, v := range sm.stats.topNget {
		println("\t", k, v)
	}
}
