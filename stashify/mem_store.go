package stashify

import (
	"encoding/binary"
	"maps"
	"slices"
	"strings"
	"sync"
	"time"

	_ "github.com/no-src/nscache/memory"
	"gitlab.com/bitify-pub/byutils/stashify/store"
)

type MemoryStore struct {
	memstore map[string]store.Data
}

func NewMemoryStore() MemoryStore {
	return MemoryStore{
		memstore: make(map[string]store.Data),
	}
}

// mutex for Put. Get also has to wait for Put to finish
var memLock = &sync.Mutex{}

func (ms *MemoryStore) Get(key string) ([]byte, bool) {
	memLock.Lock()
	defer memLock.Unlock()

	if v, ok := ms.memstore[key]; ok {
		if v.ExpireTime != nil && time.Now().After(*v.ExpireTime) {
			delete(ms.memstore, key)
			return nil, false
		}
		return v.Data, true
	}

	return nil, false
}

func (ms *MemoryStore) Put(key string, data []byte, expire time.Duration) {
	memLock.Lock()
	defer memLock.Unlock()

	var t time.Time = time.Now().Add(expire)

	ms.memstore[key] = store.Data{
		Data:       data,
		ExpireTime: &t,
	}
}

// returns all data size in bytes
func (ms *MemoryStore) ByteSize() int {
	size := 0
	for _, v := range ms.memstore {
		size += binary.Size(v.Data)
	}

	return size
}

func (ms *MemoryStore) delete(cacheKey string) {
	memLock.Lock()
	defer memLock.Unlock()

	delete(ms.memstore, cacheKey)
}

func (ms *MemoryStore) deleteWithPrefix(cachePrefix string) {
	memLock.Lock()
	defer memLock.Unlock()

	keys := slices.Collect[string](maps.Keys(ms.memstore))

	for _, k := range keys {
		if strings.HasPrefix(k, cachePrefix) {
			delete(ms.memstore, k)
		}
	}
}
