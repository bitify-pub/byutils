package stashify

import (
	"crypto/rand"
	"fmt"
	"math/big"
	"testing"
	"time"
)


func TestGet(t *testing.T) {
	t.Run("TestGet", func(t *testing.T) {
		start := time.Now()
		// get nothing
		// _, found := Get("no cache"); if found {
		// 	t.Errorf("Expected: %v, got: %v", false, found)
		// }

		// set someting
		Put("cache", []byte("data"), time.Duration(5) * time.Second)
		fmt.Println("Put 1 byte elapsed: ", time.Since(start))

		// get back
		data, found := Get("cache"); if !found {
			t.Errorf("Expected: %v, got: %v", true, found)
		}
		fmt.Println("Get 1 byte elapsed: ", time.Since(start))

		// check data
		if string(data) != "data" {
			t.Errorf("Expected: %v, got: %v", "data", string(data))
		}

		fmt.Println("TestGet took: ", time.Since(start))

		newStashManager().info()

		t.Fail()
	})
}

func TestPut(t *testing.T) {
	t.Run("TestPut", func(t *testing.T) {
		start := time.Now()

		maxMemSize = 10_000 // 1MB

		numloop := 1000000
		var numitem int64 = 50

		keys := make([]string, 0)
		// put something random 1000 times
		for i := 0; i < numloop; i++ {
			key, err := rand.Int(rand.Reader, big.NewInt(numitem))
			if err != nil {
				t.Errorf("Expected: %v, got: %v", nil, err)
			}
			strkey := fmt.Sprintf("cache%d", key)
			keys = append(keys, strkey)

			Put(strkey, []byte("The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog The quick brown fox jumps over the lazy dog"), time.Duration(10) * time.Second)
		}
		fmt.Printf("TestPut %d item, took: %v\n", numloop, time.Since(start))

		// get back
		for i := 0; i < numloop; i++ {
			index, err := rand.Int(rand.Reader, big.NewInt(int64(len(keys))))
			if err != nil {
				t.Errorf("Expected: %v, got: %v", nil, err)
			}

			_, found := Get(keys[int(index.Int64())]); if !found {
				t.Errorf("Expected: %v, got: %v", true, found)
			}
		}
		
		fmt.Printf("TestGet %d took: %v\n", numloop, time.Since(start))

		// print stat
		sm := newStashManager()
		// sm.info()
		fmt.Printf("memory usage %d bytes\n", sm.memstore.ByteSize())

		t.Fail()
	})
}