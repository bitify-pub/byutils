package stashify

import "time"

// Stash is the core interface of the cache
// (inspired by NSCache)
type Stash interface {
		// Get get cache data by key
		Get(k string, v any) error

		// Set set new cache data
		Set(k string, v any, expiration time.Duration) error
	
		// Remove remove the specified key
		Remove(k string) error
	
		// Close close the cache component
		Close() error
}