package stashify

import (
	"sync"
	"time"

	"github.com/no-src/log"
	"gitlab.com/bitify-pub/byutils/stashify/store"

	"gitlab.com/bitify-pub/byutils/stashify/boltdb"
)

type DiskStore struct {
	storeUri string
}

func NewDiskStore() DiskStore {
	return DiskStore{
		storeUri: "boltdb://boltdb.db",
	}
}

// mutex for Put. Get also has to wait for Put to finish
var diskLock = &sync.Mutex{}

func (ds *DiskStore) Get(cacheKey string) ([]byte, bool) {
	diskLock.Lock()
	defer diskLock.Unlock()

	var data []byte

	// read data from cache
	c, err := boltdb.NewBoltDBStore(ds.storeUri)
	if err != nil {
		log.Error(err, "GetFrom_boltdb: Error creating cache: ")
		return nil, false
	}
	defer c.Close()

	err = c.Get(cacheKey, &data)
	if err != nil {
		log.Debug("[CACHE MISS]: %s err:%v", cacheKey, err)
		return nil, false
	}
	log.Debug("[CACHE HIT]: %s", cacheKey)
	return data, true
}

// returns raw object as store.Data
func (ds *DiskStore) GetData(cacheKey string) *store.Data {
	c, err := boltdb.NewBoltDBStore(ds.storeUri)
	if err != nil {
		return nil
	}
	defer c.Close()

	var d *store.Data = c.GetStore().Get(cacheKey)

	if d == nil {
		return nil
	}

	return d
}

func (ds *DiskStore) Put(cacheKey string, data []byte, expire time.Duration) {
	diskLock.Lock()
	defer diskLock.Unlock()

	c, err := boltdb.NewBoltDBStore(ds.storeUri)
	if err != nil {
		log.Error(err, "Error creating cache: ")
		return
	}
	defer c.Close()

	// write data to cache
	log.Debug("Cache for %v [key: %s] - BOLTDB", expire, cacheKey)
	err = c.Set(cacheKey, data, expire) // expire after 60 seconds
	if err != nil {
		log.Error(err, "Error saving data to cache: ")
	}
}

func (ds *DiskStore) Delete(cacheKey string) {
	diskLock.Lock()
	defer diskLock.Unlock()

	c, err := boltdb.NewBoltDBStore(ds.storeUri)
	if err != nil {
		log.Error(err, "Error creating cache: ")
		return
	}
	defer c.Close()

	// delete data from cache
	err = c.Remove(cacheKey)
	if err != nil {
		log.Error(err, "Error deleting data from cache: ")
	}
}

func (ds *DiskStore) DeleteWithPrefix(cachePrefix string) {
	diskLock.Lock()
	defer diskLock.Unlock()

	c, err := boltdb.NewBoltDBStore(ds.storeUri)
	if err != nil {
		log.Error(err, "Error creating cache: ")
		return
	}
	defer c.Close()

	// delete data from cache
	err = c.RemoveWithPrefix(cachePrefix)
	if err != nil {
		log.Error(err, "Error deleting data from cache: ")
	}
}
