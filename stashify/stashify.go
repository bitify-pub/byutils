package stashify

// Cache implementation using nscache and boltdb

import (
	"time"

	_ "github.com/no-src/nscache/boltdb"
)

var DevMode = false

func Get(cacheKey string) ([]byte, bool) {
	sm := newStashManager()
	return sm.get(cacheKey)
}

// Put60 sets data to cache with 60 seconds expiry
func PutWithExpiry60s(cacheKey string, data []byte) {
	Put(cacheKey, data, 60*time.Second)
}

// cache data only if if does not exist for specified duration.
// this method is thread safe
func Put(cacheKey string, data []byte, expire time.Duration) {
	cm := newStashManager()
	cm.put(cacheKey, data, expire)
}

// Clear clears the cache for the given key
func Clear(cacheKey string) {
	sm := newStashManager()
	sm.delete(cacheKey)
}

func ClearWithPrefix(cachePrefix string) {
	sm := newStashManager()
	sm.deleteWithPrefix(cachePrefix)

}
