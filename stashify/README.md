# Stashify

Wrapper around NSCACHE, to make it easier to work with.

Simply use `Put` and `Get` for adding and getting data to cache.

The objective is not to use in-memory cache to avoid excessive memory usage.

Cache are stored to disk. Nowadays, SSD volume are pretty reliable in speed to work as cache.

By default it uses `boltdb` and set up a file named `boltdb.db` at working directory to be used as cached storage. Alternatively, it is also set up to use `Etcd` in case one has been set up.
